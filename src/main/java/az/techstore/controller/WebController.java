package az.techstore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class WebController {

    @GetMapping("/")
    public String index() {
        return "web/index";
    }

    @GetMapping("/account-signing")
    public String registerLogin() {
        return "web/recover-account";
    }

    @GetMapping("/recover-account")
    public String recoverAccount() {
        return "web/recover-account";
    }




}
