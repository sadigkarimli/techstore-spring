<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<meta charset="utf-8">
<!-- SEO Meta Tags-->
<meta name="description" content="MStore - Modern Bootstrap E-commerce Template">
<meta name="keywords" content="bootstrap, shop, e-commerce, market, modern, responsive,  business, mobile, bootstrap 4, html5, css3, jquery, js, gallery, slider, touch, creative, clean">
<meta name="author" content="Createx Studio">
<!-- Viewport-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="msapplication-TileColor" content="#111">
<meta name="theme-color" content="#ffffff">